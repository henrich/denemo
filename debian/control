Source: denemo
Maintainer: Dr. Tobias Quathamer <toddy@debian.org>
Uploaders: Anthony Fok <foka@debian.org>
Section: sound
Priority: optional
Build-Depends: autoconf,
               automake,
               autopoint,
               debhelper-compat (= 12),
               gtk-doc-tools,
               guile-2.2-dev,
               intltool,
               libaubio-dev,
               libevince-dev,
               libfftw3-dev,
               libfluidsynth-dev,
               libgtk-3-dev,
               libgtksourceview-3.0-dev,
               libportmidi-dev [linux-any],
               librsvg2-dev,
               librubberband-dev,
               libsamplerate0-dev,
               libsmf-dev,
               libsndfile1-dev,
               libtool,
               libxml2-dev,
               lilypond,
               portaudio19-dev
Build-Conflicts: autoconf2.13,
                 automake1.4
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/debian/denemo
Vcs-Git: https://salsa.debian.org/debian/denemo.git
Homepage: http://www.denemo.org

Package: denemo
Architecture: any
Depends: denemo-data (= ${source:Version}),
         ttf-denemo (= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: alsa,
            denemo-doc (= ${source:Version}),
            lilypond
Description: GTK+ front end to GNU Lilypond
 GNU Denemo is a GUI musical score editor written in C/GTK+. It is
 intended primarily as a front end to GNU Lilypond, but is adaptable to
 other computer-music-related purposes as well.

Package: denemo-data
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: data for denemo
 GNU Denemo is a GUI musical score editor written in C/GTK+. It is
 intended primarily as a front end to GNU Lilypond, but is adaptable to
 other computer-music-related purposes as well.
 .
 This package contains the arch-independent files.

Package: denemo-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends}
Description: documentation and examples for denemo
 GNU Denemo is a GUI musical score editor written in C/GTK+. It is
 intended primarily as a front end to GNU Lilypond, but is adaptable to
 other computer-music-related purposes as well.
 .
 This package contains the HTML documentation and examples.

Package: ttf-denemo
Architecture: all
Multi-Arch: foreign
Section: fonts
Depends: ${misc:Depends}
Description: music notation symbol fonts for denemo
 GNU Denemo is a GUI musical score editor written in C/GTK+. It is
 intended primarily as a front end to GNU Lilypond, but is adaptable to
 other computer-music-related purposes as well.
 .
 This package contains the Music Notation Symbol Fonts.
